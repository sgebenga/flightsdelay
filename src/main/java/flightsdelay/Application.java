package flightsdelay;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.security.PrivilegedAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application extends Configured implements Tool {

    private final static Logger LOGGER = Logger.getLogger(String.valueOf(Application.class));

    public static void main(String[] args) throws Exception {
        int status = ToolRunner.run(new Configuration(), new Application(), args);
        System.exit(status);
    }

    public int run(final String[] args) throws Exception {
        UserGroupInformation ugi = UserGroupInformation.createRemoteUser("root");

        if (args.length != 3) {
            System.out.println("Three params are required <weather-dir> <input-dir> and <output-dir>");
            return -1;
        }
        Integer result = ugi.doAs(new PrivilegedAction<Integer>() {
            public Integer run() {
                try {
                    return innerRun(args);
                } catch (Exception e) {
                    LOGGER.log(Level.OFF, "Global exception", e);
                    return -1;
                }
            }
        });
        return result;
    }

    private int innerRun(final String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration configuration = getConf();
        // configuration.set("fs.defaultFS", "hdfs://sandbox.hortonworks.com:8020");
        configuration.set("hadoop.job.ugi", "root");

        Job job = Job.getInstance(configuration, "flights-delay application");
        Path cachePath = new Path(args[0]);
        FileSystem fsy = FileSystem.get(configuration);
        FileStatus status = fsy.getFileStatus(cachePath);
        //LOG owner and name , permission

        Path inPath = new Path(args[1]);
        Path outPath = new Path(args[2]);
        if (fsy.exists(outPath)) {
            fsy.delete(outPath, true);
        }

        job.setCacheFiles(new URI[]{cachePath.toUri()});
        FileInputFormat.addInputPath(job, inPath);
        FileOutputFormat.setOutputPath(job, outPath);

        job.setMapperClass(FlightsMapper.class);
        job.setReducerClass(FlightsReducer.class);

        job.setOutputKeyClass(Key.class);
        job.setOutputValueClass(Value.class);

        job.setNumReduceTasks(2);

        return job.waitForCompletion(true) ? 0 : 1;
    }


}
