package flightsdelay;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Value implements Writable {

    private int year;
    private int month;
    private int day;
    private int depTime;
    private int arrTime;
    private Text uniqueCarrier;
    private int flightNumber;
    private int actualElapsedTime;
    private int arrDelay;
    private int depDelay;
    private Text origin;
    private Text destination;
    private int prcp;
    private int tmax;
    private int tmin;


    public Value() {
        set(new Text(), new Text(), new Text());
    }

    private void set(Text uniqueCarrier, Text origin, Text destination) {
        this.uniqueCarrier = uniqueCarrier;
        this.origin = origin;
        this.destination = destination;
    }

    public void write(DataOutput output) throws IOException {
        output.writeInt(year);
        output.writeInt(month);
        output.writeInt(day);
        output.writeInt(depTime);
        output.writeInt(arrTime);
        uniqueCarrier.write(output);
        output.writeInt(flightNumber);
        output.writeInt(actualElapsedTime);
        output.writeInt(arrTime);
        output.writeInt(depDelay);
        origin.write(output);
        destination.write(output);
        output.writeInt(prcp);
        output.writeInt(tmax);
        output.writeInt(tmin);
    }

    public void readFields(DataInput input) throws IOException {
        year = input.readInt();
        month = input.readInt();
        day = input.readInt();
        depTime = input.readInt();
        arrTime = input.readInt();
        uniqueCarrier.readFields(input);
        flightNumber = input.readInt();
        actualElapsedTime = input.readInt();
        arrTime = input.readInt();
        depDelay = input.readInt();
        origin.readFields(input);
        destination.readFields(input);
        prcp = input.readInt();
        tmax = input.readInt();
        tmin = input.readInt();
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setDepTime(int depTime) {
        this.depTime = depTime;
    }

    public void setArrTime(int arrTime) {
        this.arrTime = arrTime;
    }

    public void setUniqueCarrier(Text uniqueCarrier) {
        this.uniqueCarrier = uniqueCarrier;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setActualElapsedTime(int actualElapsedTime) {
        this.actualElapsedTime = actualElapsedTime;
    }

    public void setArrDelay(int arrDelay) {
        this.arrDelay = arrDelay;
    }

    public void setDepDelay(int depDelay) {
        this.depDelay = depDelay;
    }

    public void setOrigin(Text origin) {
        this.origin = origin;
    }

    public void setDestination(Text destination) {
        this.destination = destination;
    }

    public void setPrcp(int prcp) {
        this.prcp = prcp;
    }

    public void setTmax(int tmax) {
        this.tmax = tmax;
    }

    public void setTmin(int tmin) {
        this.tmin = tmin;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getDepTime() {
        return depTime;
    }

    public int getArrTime() {
        return arrTime;
    }

    public Text getUniqueCarrier() {
        return uniqueCarrier;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public int getActualElapsedTime() {
        return actualElapsedTime;
    }

    public int getArrDelay() {
        return arrDelay;
    }

    public int getDepDelay() {
        return depDelay;
    }

    public Text getOrigin() {
        return origin;
    }

    public Text getDestination() {
        return destination;
    }

    public int getPrcp() {
        return prcp;
    }

    public int getTmax() {
        return tmax;
    }

    public int getTmin() {
        return tmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value = (Value) o;

        if (year != value.year) return false;
        if (month != value.month) return false;
        if (day != value.day) return false;
        if (depTime != value.depTime) return false;
        if (arrTime != value.arrTime) return false;
        if (flightNumber != value.flightNumber) return false;
        if (actualElapsedTime != value.actualElapsedTime) return false;
        if (arrDelay != value.arrDelay) return false;
        if (depDelay != value.depDelay) return false;
        if (prcp != value.prcp) return false;
        if (tmax != value.tmax) return false;
        if (tmin != value.tmin) return false;
        if (!uniqueCarrier.equals(value.uniqueCarrier)) return false;
        if (!origin.equals(value.origin)) return false;
        return destination.equals(value.destination);
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + month;
        result = 31 * result + day;
        result = 31 * result + depTime;
        result = 31 * result + arrTime;
        result = 31 * result + uniqueCarrier.hashCode();
        result = 31 * result + flightNumber;
        result = 31 * result + actualElapsedTime;
        result = 31 * result + arrDelay;
        result = 31 * result + depDelay;
        result = 31 * result + origin.hashCode();
        result = 31 * result + destination.hashCode();
        result = 31 * result + prcp;
        result = 31 * result + tmax;
        result = 31 * result + tmin;
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", year, month, day, depTime, arrTime, uniqueCarrier, flightNumber, actualElapsedTime, arrDelay, depDelay, origin, destination, prcp, tmax, tmin);
    }

}


