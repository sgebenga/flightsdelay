package flightsdelay;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class FlightsMapper extends Mapper<LongWritable, Text, Key, Value> {

    private static String SFO = "sfo";

    private static Map<Key, WeatherInformation> weatherMap = new HashMap<Key, WeatherInformation>();

    public enum Counters {
        HITS, SAN_FRANCISCO
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        URI[] cache = context.getCacheFiles();
        if (cache.length > 0) {
            Path cachePath = new Path(cache[0]);
            FileSystem fs = cachePath.getFileSystem(context.getConfiguration());
            FileStatus[] statuses = fs.listStatus(cachePath);
            for (FileStatus status : statuses) {
                loadWeatherFile(status, fs, context);
            }
        }
    }

    private void loadWeatherFile(FileStatus status, FileSystem fs, Context context) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(fs.open(status.getPath())));
            String line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] splits = line.split(",");
                if (splits.length > 6) {
                    Key key = new Key(parseInt(splits[1]), parseInt(splits[2]), parseInt(splits[3]));
                    WeatherInformation weatherInformation = new WeatherInformation(parseInt(splits[4]), parseInt(splits[5]), parseInt(splits[6]));
                    weatherMap.put(key, weatherInformation);
                    context.getCounter(Counters.SAN_FRANCISCO).increment(1);
                }
            }
        } catch (Exception ex) {

        }
    }


    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] splits = value.toString().split(",");
        if (SFO.equalsIgnoreCase(splits[16])) {
            Key k = new Key(parseInt(splits[0]), parseInt(splits[1]), parseInt(splits[2]));
            WeatherInformation weatherInformation = weatherMap.get(k);
            if (weatherInformation != null) {
                Value v = buildValue(splits, weatherInformation);
                k.setArrDelay(v.getArrDelay());
                context.write(k, v);
                context.getCounter(Counters.HITS).increment(1);
            }
        }
    }

    private Value buildValue(String[] s, WeatherInformation wi) {
        Value v = new Value();
        v.setYear(parseInt(s[0]));
        v.setMonth(parseInt(s[1]));
        v.setDay(parseInt(s[2]));
        v.setDepTime(parseInt(s[4]));
        v.setArrTime(parseInt(s[6]));
        v.setUniqueCarrier(new Text(s[8]));
        v.setFlightNumber(parseInt(s[9]));
        v.setActualElapsedTime(parseInt(s[11]));
        v.setArrDelay(parseInt(s[14]));
        v.setDepDelay(parseInt(s[15]));
        v.setOrigin(new Text(s[16]));
        v.setDestination(new Text(s[17]));

        v.setPrcp(wi.getPrcp());
        v.setTmax(wi.getTmax());
        v.setTmin(wi.getTmin());
        return v;
    }

    private int parseInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }
}
