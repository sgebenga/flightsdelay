package flightsdelay;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class FlightsReducer extends Reducer<Key, Value, Value, Text> {

    private final static Text EMPTY = new Text(""); /// Replace with NullWritable

    @Override
    protected void reduce(Key key, Iterable<Value> values, Context context) throws IOException, InterruptedException {
        for (Value value : values) {
            context.write(value, EMPTY);
        }
    }
}
