package flightsdelay;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Key implements WritableComparable<Key> {

    private int year;
    private int month;
    private int day;
    private int arrDelay;

    public Key() {
    }

    public Key(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void write(DataOutput output) throws IOException {
        output.writeInt(year);
        output.writeInt(month);
        output.writeInt(day);
        output.writeInt(day);
    }

    public void readFields(DataInput input) throws IOException {
        year = input.readInt();
        month = input.readInt();
        day = input.readInt();
        arrDelay = input.readInt();
    }

    public int compareTo(Key o) {
        long thisDate = Long.parseLong(String.format("%s%s%s", year, padZero(month), padZero(day)));
        long otherDate = Long.parseLong(String.format("%s%s%s", o.year, padZero(o.month), padZero(o.day)));
        int cmp = (int) (thisDate - otherDate);
        return (cmp == 0) ? (arrDelay - o.arrDelay) : cmp;
    }

    private String padZero(int i) {
        String out = "";
        if (i < 10) {
            out = "0" + i;
        }
        return out;

    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getArrDelay() {
        return arrDelay;
    }

    public void setArrDelay(int arrDelay) {
        this.arrDelay = arrDelay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key key = (Key) o;

        if (year != key.year) return false;
        if (month != key.month) return false;
        if (day != key.day) return false;
        return arrDelay == key.arrDelay;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + month;
        result = 31 * result + day;
        result = 31 * result + arrDelay;
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%s,%s", year, month, day, arrDelay);
    }
}
