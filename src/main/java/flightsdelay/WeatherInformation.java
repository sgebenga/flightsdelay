package flightsdelay;

public class WeatherInformation {

    private int prcp;
    private int tmax;
    private int tmin;

    public WeatherInformation() {

    }

    public WeatherInformation(int prcp, int tmax, int tmin) {
        this.prcp = prcp;
        this.tmax = tmax;
        this.tmin = tmin;
    }

    public int getPrcp() {
        return prcp;
    }

    public int getTmax() {
        return tmax;
    }

    public int getTmin() {
        return tmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherInformation that = (WeatherInformation) o;

        if (prcp != that.prcp) return false;
        if (tmax != that.tmax) return false;
        return tmin == that.tmin;
    }

    @Override
    public int hashCode() {
        int result = prcp;
        result = 31 * result + tmax;
        result = 31 * result + tmin;
        return result;
    }

    @Override
    public String toString() {
        return "WeatherInformation{" +
                "prcp=" + prcp +
                ", tmax=" + tmax +
                ", tmin=" + tmin +
                '}';
    }
}
